package com.training.wallclient.api;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class ApiFactory {

    private ApiFactory() {
    }

    public static WallServerAPI create() {
        return getBuilder()
                .build().create(WallServerAPI.class);
    }

    public static RestAdapter.Builder getBuilder() {
        return new RestAdapter.Builder()
                .setServer("http://10.0.0.13:8000/")
                .setConverter(new GsonConverter(
                        getGson()
                ))
                .setLogLevel(RestAdapter.LogLevel.FULL);
    }

    public static Gson getGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
    }
}
