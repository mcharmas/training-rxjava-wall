package com.training.wallclient;

import android.app.Notification;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.training.wallclient.api.ApiFactory;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class NetworkService extends RetrofitGsonSpiceService {

    @Override
    protected RestAdapter.Builder createRestAdapterBuilder() {
        return ApiFactory.getBuilder();
    }

    @Override
    protected String getServerUrl() {
        return null;
    }

    @Override
    public Notification createDefaultNotification() {
        return null;
    }
}
