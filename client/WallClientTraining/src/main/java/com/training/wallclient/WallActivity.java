package com.training.wallclient;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

import com.training.wallclient.api.ApiFactory;
import com.training.wallclient.api.WallServerAPI;
import com.training.wallclient.model.Post;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WallActivity extends BaseGMSActivity implements Callback<Response> {

    private EditText messageView;
    private WallServerAPI wallServerAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wall);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new WallFragment())
                    .commit();
        }

        wallServerAPI = ApiFactory.create();
        messageView = (EditText) findViewById(R.id.message_edit_text);
        findViewById(R.id.send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
    }

    private void sendMessage() {
        Editable text = messageView.getText();
        if (text != null && text.length() != 0 && text.toString().trim().length() != 0) {
            wallServerAPI.publishPost(new Post(text.toString()), this);
            messageView.setText("");
        }
    }

    @Override
    protected void onPlayServicesReady() {
        if (!GCMUtils.isRegistered(this)) {
            startService(new Intent(this, GCMRegisterService.class));
        }
    }

    @Override
    public void success(Response response, Response response2) {

    }

    @Override
    public void failure(RetrofitError error) {

    }
}
