package com.training.wallclient;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.training.wallclient.api.ApiFactory;
import com.training.wallclient.api.WallServerAPI;
import com.training.wallclient.model.Post;
import com.training.wallclient.model.PostsAdapter;

public class WallFragment extends ListFragment {

    public static final String POSTS_CACHE_KEY = "posts";
    private SpiceManager spiceManager = new SpiceManager(NetworkService.class);
    private WallUpdatedBroadcastReceiver receiver;
    private Post.List postsData;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        spiceManager.start(getActivity());
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        spiceManager.shouldStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        performRequest();
        receiver = new WallUpdatedBroadcastReceiver();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter(GcmBroadcastReceiver.ACTION_NEW_POST_ARRIVED));
    }

    private void performRequest() {
        spiceManager.execute(new AllPostsSpiceRequest(), POSTS_CACHE_KEY, DurationInMillis.ONE_HOUR, new AllPostsRequestListener());
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    private static class AllPostsSpiceRequest extends RetrofitSpiceRequest<Post.List, WallServerAPI>{

        public AllPostsSpiceRequest() {
            super(Post.List.class, WallServerAPI.class);
        }

        @Override
        public Post.List loadDataFromNetwork() throws Exception {
            return getService().getAllPosts();
        }
    }

    private class AllPostsRequestListener implements RequestListener<Post.List> {

        @Override
        public void onRequestFailure(SpiceException e) {
            Toast.makeText(getActivity(), "Terrible failure getting postsData.", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(Post.List posts) {
            postsData = posts;
            setListAdapter(new PostsAdapter(getActivity(), posts));
        }
    }

    private class WallUpdatedBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Post post = ApiFactory.getGson().fromJson(intent.getStringExtra("data"), Post.class);
            postsData.add(0, post);
            ((ArrayAdapter<?>)getListAdapter()).notifyDataSetChanged();
            spiceManager.removeDataFromCache(Post.List.class, POSTS_CACHE_KEY);
        }
    }

}
