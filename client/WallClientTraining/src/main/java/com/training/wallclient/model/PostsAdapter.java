package com.training.wallclient.model;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.training.wallclient.R;

import java.util.List;

public class PostsAdapter extends ArrayAdapter<Post> {
    public PostsAdapter(Context context, List<Post> objects) {
        super(context, R.layout.post_item, R.id.post_content_view, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        ((TextView)view.findViewById(R.id.post_content_date)).setText(getItem(position).getPublicationDate().toString());
        return view;
    }
}
