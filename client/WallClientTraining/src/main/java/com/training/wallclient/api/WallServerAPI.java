package com.training.wallclient.api;

import com.training.wallclient.model.Device;
import com.training.wallclient.model.Post;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

public interface WallServerAPI {

    @POST("/wall/devices/?format=json")
    Response registerDevice(@Body Device device);

    @POST("/wall/posts/")
    void publishPost(@Body Post post, Callback<Response> cb);

    @GET("/wall/posts/")
    Post.List getAllPosts();

}
