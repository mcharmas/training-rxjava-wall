package com.training.wallclient;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.training.wallclient.api.ApiFactory;
import com.training.wallclient.model.Device;

import java.io.IOException;

import retrofit.RetrofitError;

public class GCMRegisterService extends IntentService {

    private static final String SENDER_ID = "576469651759";

    public GCMRegisterService() {
        super("GCMRegisterWorker");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            String regid = gcm.register(SENDER_ID);
            sendRegistrationIdToBackend(regid);
            GCMUtils.storeRegistrationId(this, regid);
        } catch (IOException ex) {
            // here we should retry the attempt to register
            throw new IllegalStateException("Error registering to GCM.", ex);
        }
    }

    private void sendRegistrationIdToBackend(String regid) {
        try {
            ApiFactory.create().registerDevice(new Device(regid));
        } catch (RetrofitError error) {
            Log.e("GCMRegisterService", "Error", error);
        }
    }
}
