package com.training.wallclient.model;

import java.util.ArrayList;
import java.util.Date;

public class Post {
    public static class List extends ArrayList<Post> {}

    private String content;
    private Date publicationDate;

    public Post(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return getContent();
    }

    public Date getPublicationDate() {
        return publicationDate;
    }
}
