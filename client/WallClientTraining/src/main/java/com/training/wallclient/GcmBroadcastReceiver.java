package com.training.wallclient;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class GcmBroadcastReceiver extends BroadcastReceiver {

    public static final String ACTION_NEW_POST_ARRIVED = "ACTION_NEW_POST_ARRIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                new Intent(ACTION_NEW_POST_ARRIVED).putExtras(intent)
        );
    }
}
