# Aplikacja pozwalająca na publikowanie przez API na wspólnej ścianie

Każda opublikowana wiadomość powiadamia serwer google cloud messaging.
Project ID dla GCM: *576469651759*

Uruchamianie serwera:
```./manage.py runserver 0.0.0.0:8000```

## API Serwera:

### Posty

#### Pobieranie listy
```GET /wall/posts/``` - zwraca listę wszystki postów

Przykład:
```
[
    {
        "id": 1, 
        "publication_date": "2013-09-03T16:04:39.532Z", 
        "content": "This is sample post."
    }
]
```

#### Publikowanie postu
```POST /wall/posts/``` - zwracany status *201 Created*

Przykład wysyłanej treści:
```
{ "content": "another sample post" }
```

### Zarządzanie urządzeniami:
```GET /wall/devices/``` oraz ```POST /wall/devices/```

## PUSH

Wiadomosc z serwera przychodzi z danymi. Dane znajduja sie pod kluczem ```data```.
Dane sa stringiem w formacie JSON (dokladnie takim samym, jak pojedyncza wiadomosc).
