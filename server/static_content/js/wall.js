angular.module('wall', ["ngResource"])
    .factory('Post', function ($resource) {
        return $resource('/wall/posts/', {}, {
            create: {method: 'POST'}
        })
    })
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });

                    event.preventDefault();
                }
            });
        };
    });

function PostsController($scope, Post, $timeout) {
    $scope.posts = Post.query();
    $scope.send = function () {
        var post = new Post();
        post.content = $scope.message_content;
        post.$create(function () {
            $scope.posts = Post.query();
            $scope.message_content = "";
        });
    };

    (function tick() {
        var data = Post.query(function () {
            $scope.posts = data;
            $timeout(tick, 3000);
        });
    })();
}