from rest_framework import serializers
from wall.models import WallPost, AndroidDevice


class WallPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = WallPost


class AndroidDeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AndroidDevice