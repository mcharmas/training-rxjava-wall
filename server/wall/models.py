from django.db import models

# Create your models here.
from django.db.models.fields import DateTimeField, TextField, CharField


class WallPost(models.Model):
    publication_date = DateTimeField(auto_now=True)
    content = TextField(null=False, blank=False)


class AndroidDevice(models.Model):
    creation_date = DateTimeField(auto_now=True)
    device_id = CharField(max_length=200, null=False, blank=False)
