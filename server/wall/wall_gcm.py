import datetime
from django.conf import settings
from gcm import GCM
from rest_framework.renderers import JSONRenderer
from wall.models import AndroidDevice
from wall.serializers import WallPostSerializer


class GCMMessageSender(object):
    def send_message(self, data, queryset):
        if queryset.count():
            gcm = GCM(settings.GCM_API_KEY)
            reg_ids = [device.device_id for device in queryset.all()]
            response = gcm.json_request(registration_ids=reg_ids, data=data)
            # Handling errors
            if 'errors' in response:
                for error, reg_ids in response['errors'].items():
                    # Check for errors and act accordingly
                    if error is 'NotRegistered' or 'InvalidRegistration':
                        # Remove reg_ids from database
                        for reg_id in reg_ids:
                            AndroidDevice.objects.filter(device_id=reg_id).delete()
            if 'canonical' in response:
                for canonical_id, reg_id in response['canonical'].items():
                    # Repace reg_id with canonical_id in your database
                    entry = AndroidDevice.objects.filter(device_id=reg_id).get()
                    entry.registration_id = canonical_id
                    entry.save()

    def broadcast_new_post(self, post):
        queryset = AndroidDevice.objects.all()
        json_result = JSONRenderer().render(WallPostSerializer(post).data)
        self.send_message({"data": json_result}, queryset)