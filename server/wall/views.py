# Create your views here.
from rest_framework import viewsets
from wall.wall_gcm import GCMMessageSender
from wall.models import AndroidDevice, WallPost
from wall.serializers import AndroidDeviceSerializer, WallPostSerializer


class AndroidDeviceViewSet(viewsets.ModelViewSet):
    queryset = AndroidDevice.objects.all()
    serializer_class = AndroidDeviceSerializer


class WallPostViewSet(viewsets.ModelViewSet):
    queryset = WallPost.objects.order_by('-publication_date').all()[:50]
    serializer_class = WallPostSerializer

    def post_save(self, obj, created=False):
        super(WallPostViewSet, self).post_save(obj, created)
        GCMMessageSender().broadcast_new_post(obj)

