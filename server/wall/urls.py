from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView
from rest_framework.routers import DefaultRouter

# Create a router and register our viewsets with it.
from wall import views

router = DefaultRouter()
router.register(r'devices', views.AndroidDeviceViewSet)
router.register(r'posts', views.WallPostViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
)