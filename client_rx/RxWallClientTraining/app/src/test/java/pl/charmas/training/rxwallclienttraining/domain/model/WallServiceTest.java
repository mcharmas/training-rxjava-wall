package pl.charmas.training.rxwallclienttraining.domain.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import pl.charmas.training.rxwallclienttraining.api.WallServerAPI;
import pl.charmas.training.rxwallclienttraining.domain.WallService;

import static org.mockito.Mockito.doReturn;

public class WallServiceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    WallServerAPI mockApi;
    WallService wallService;

    @Before
    public void setUp() throws Exception {
        wallService = new WallService(mockApi);
    }

    @Test
    public void fetchesPostListAfterSubscription() throws Exception {
        //given:
        List<Post> postList = Collections.emptyList();
        doReturn(Single.just(postList)).when(mockApi).getAllPosts();

        //when:
        TestObserver<List<Post>> testObserver = wallService.observeWallPosts().test();

        //then:
        testObserver.assertValue(postList);
        testObserver.assertNotComplete();
    }

    @Test
    public void refreshesPostListAfter5Seconds() throws Exception {
        //given:

        //when:

        //then:
        Assert.fail("TODO");
    }

    @Test
    public void postingAPostRefreshesPostList() throws Exception {
        //given:

        //when:

        //then:
        Assert.fail("TODO");
    }
}
