package pl.charmas.training.rxwallclienttraining.api;


import java.util.List;

import io.reactivex.Single;
import pl.charmas.training.rxwallclienttraining.domain.model.Post;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface WallServerAPI {
    @POST("/wall/posts/")
    Single<Post> publishPost(@Body Post post);

    @GET("/wall/posts/")
    Single<List<Post>> getAllPosts();
}
