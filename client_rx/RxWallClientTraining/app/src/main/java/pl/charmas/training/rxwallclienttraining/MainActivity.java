package pl.charmas.training.rxwallclienttraining;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.charmas.training.rxwallclienttraining.api.ApiFactory;
import pl.charmas.training.rxwallclienttraining.domain.WallService;
import pl.charmas.training.rxwallclienttraining.domain.model.Post;


public class MainActivity extends AppCompatActivity {
    @BindView(R.id.messages_list)
    ListView messageListView;

    @BindView(R.id.send_button)
    Button sendButton;

    @BindView(R.id.message_content)
    EditText messageContentView;

    @BindView(R.id.address_button)
    Button addressButton;

    private WallService wallService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wallService = new WallService(ApiFactory.create());

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        addressButton.setOnClickListener(v -> startActivityForResult(new Intent(MainActivity.this, AddressActivity.class), 0));
    }


    private void enableAndClearPostInputs() {
        messageContentView.setText("");
        messageContentView.setEnabled(true);
    }

    private void disablePostInputs() {
        messageContentView.setEnabled(false);
        sendButton.setEnabled(false);
    }

    private void presentPostList(List<Post> posts) {
        PostsAdapter adapter = (PostsAdapter) messageListView.getAdapter();
        if (messageListView.getAdapter() != null) {
            adapter.swapData(posts);
        } else {
            adapter = new PostsAdapter(MainActivity.this, posts);
            messageListView.setAdapter(adapter);
        }
    }

    private void presentError(Throwable throwable) {
        Toast.makeText(this, "Error loading data.", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            messageContentView.setText(data.getStringExtra(AddressActivity.RESULT_ADDRESS));
        }
    }
}
