package pl.charmas.training.rxwallclienttraining.domain;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import pl.charmas.training.rxwallclienttraining.api.WallServerAPI;
import pl.charmas.training.rxwallclienttraining.domain.model.Post;

public class WallService {
    private final WallServerAPI serverAPI;

    public WallService(WallServerAPI serverAPI) {
        this.serverAPI = serverAPI;
    }

    public Observable<List<Post>> observeWallPosts() {
        //TODO
        return Observable.empty();
    }

    public Completable publishPost(Post post) {
        //TODO
        return Completable.complete();
    }
}
