package pl.charmas.training.rxwallclienttraining;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class AddressActivity extends ActionBarActivity {
    public static final String RESULT_ADDRESS = "RESULT_ADDRESS";
    private EditText searchInput;
    private ListView addressResultList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        searchInput = (EditText) findViewById(R.id.search_input);
        addressResultList = (ListView) findViewById(R.id.address_results_list);
        addressResultList.setOnItemClickListener((parent, view, position, id) -> {
            String address = (String) parent.getAdapter().getItem(position);
            setResult(Activity.RESULT_OK, new Intent().putExtra(RESULT_ADDRESS, address));
            finish();
        });
    }

    private void presentAddresses(List<String> addresses) {
        addressResultList.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, addresses));
    }
}
