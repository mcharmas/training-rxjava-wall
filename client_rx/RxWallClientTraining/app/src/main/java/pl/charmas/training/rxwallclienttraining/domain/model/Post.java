package pl.charmas.training.rxwallclienttraining.domain.model;

import java.util.Date;

public class Post {
    private final String content;
    private final Date publicationDate;

    public Post(String content) {
        this.content = content;
        this.publicationDate = new Date();
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return getContent();
    }

    public Date getPublicationDate() {
        return publicationDate;
    }
}
